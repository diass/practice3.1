package practice3.task.domain;

public class Circle extends Shape {
    private double radius;

    public Circle(){
        setRadius(1.0);
    }

    public Circle(double radius){
        setRadius(radius);
    }

    public Circle(Color color, boolean filled, double radius){
        super(color, filled);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "A circle with radius = " + radius + ", which is a subclass of " + super.toString();
    }
}
