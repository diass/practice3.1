package practice3.task.domain;

public enum Color {
    RED("Red"),
    GREEN("Green"),
    BLACK("Black"),
    WHITE("White"),
    YELLOW("Yellow"),
    BLUE("Blue"),
    PINK("Pink");

    private String color;

    Color(String color){
        setColor(color);
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getColor(){
        return color;
    }


    @Override
    public String toString() {
        return color;
    }
}
