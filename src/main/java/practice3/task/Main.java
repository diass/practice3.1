package practice3.task;

import org.w3c.dom.ls.LSOutput;
import practice3.task.domain.Circle;
import practice3.task.domain.Color;

public class Main {

    public static void main(String[] args){
        Circle circle = new Circle(Color.BLACK, false, 6.7);
        System.out.println(circle);
    }
}
